//
// Created by Esperanza on 18/01/2020.
//

#ifndef BASICS_PROJECT_TEMPLATE_MENU_SCENE_HPP
#define BASICS_PROJECT_TEMPLATE_MENU_SCENE_HPP


#include <memory>
#include <basics/Scene>
#include <basics/Atlas>
#include <basics/Canvas>
#include <basics/Point>
#include <basics/Size>



namespace shooter
{
    using basics::Atlas;
    using basics::Canvas;
    using basics::Point2f;
    using basics::Size2f;
    using basics::Texture_2D;
    using basics::Graphics_Context;


    class Menu_Scene : public basics::Scene
    {

        ///estado de la escena
        enum State
        {
            LOADING,
            READY,
            ERROR

        };

        ///opciones posibles del menú
        enum Option_id
        {
            PLAY,
            HELP,
        };

        ///Características de una opción
        struct Option
        {
            const Atlas::Slice *slice;
            Point2f position;
            bool is_pressed;

        };

        struct TitleSlice
        {
            const Atlas::Slice *slice;
            Point2f position;
        };



        ///tamaño del array de opciones
        static const unsigned options_size = 2;
        static const unsigned title_slices = 2;

    private:

        ///variable por la que se designa el estado actual
        State state;

        ///variable para controlar si la aplicación está en segundo plano
        bool suspended;

        ///medidas iniciales del canvas
        unsigned canvas_width;
        unsigned canvas_height;

        ///creamos el array de opciones con su tamaño
        Option options_array[options_size];

        ///creamos el array de palabras del título con su tamaño
        TitleSlice slices [title_slices];

        ///creamos el atlas del que sacaremos los datos del menú
        std::unique_ptr <Atlas> MyAtlas;
        std::unique_ptr <Atlas> MyTitleAtlas;


        //CONFIGURACIÓN DEL ASPECT RATIO
        bool     aspect_ratio_adjusted;             /// false hasta que se ajuste el aspect ratio de la resolución
                                                    ///virtual para que concincida con el de la real


    public:

        ///constructor
        Menu_Scene();

        ///tamaño del canvas
        basics::Size2u get_view_size() override
        {
            return{canvas_width,canvas_height};
        }

        ///inicializar variables
        bool initialize() override;

        ///suspender la aplicación
        void suspend() override
        {
            suspended = true;
        };

        ///reanudar la aplicación
        void resume() override
        {
            suspended = false;
        }


        ///control de eventos del usuario
        void handle(basics::Event &event) override;

        ///actualización continua del juego
        void update(float time) override;

        ///actualización continua de los gráficos
        void render(Graphics_Context::Accessor &context) override;


    private:

        ///configuración de las teclas del menú
        void configure_menu();

        ///detecta si se está pulsando alguna opción del menú y devuelve cuál
        int point_at(Point2f &point);

    };

}
#endif //BASICS_PROJECT_TEMPLATE_MENU_SCENE_HPP