//
// Created by Esperanza on 26/01/2020.
//

#ifndef REAL_NIGHTMARES_HELP_SCENE_HPP
#define REAL_NIGHTMARES_HELP_SCENE_HPP

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Raster_Font>

namespace shooter
{
    using basics::Canvas;
    using basics::Texture_2D;
    using basics::Graphics_Context;

    class Help_Scene : public basics::Scene
    {
    private:

        //AÑADIDO PARA EL ASPECT RATIO
        enum State
        {
            LOADING,
            READY,
        };

        State    state;

        //

        bool        suspended;                      ///< true cuando la aplicaciÃ³n estÃ¡ en segundo plano
        unsigned    canvas_width;                   ///< ResoluciÃ³n virtual del display
        unsigned    canvas_height;

        //CONFIGURACIÓN DEL ASPECT RATIO

        bool     aspect_ratio_adjusted;             ///< false hasta que se ajuste el aspect ratio de la resoluciÃ³n
        ///< virtual para que concincida con el de la real
        //
        //CONFIGURACIÓN DE LA FUENTE
        typedef std::unique_ptr< basics::Raster_Font > Font_Handle;
        Font_Handle font;                           //fuente de los textos
        float margin = 50.f;                       //margen para los textos

        //texturas
        std::shared_ptr < Texture_2D > enemy1_texture;        ///< Textura que contiene la imagen del logo.
        std::shared_ptr < Texture_2D > enemy2_texture;        ///< Textura que contiene la imagen del logo.




    public:


        Help_Scene()
        {
            canvas_width  = 1280;                   // TodavÃ­a no se puede calcular el aspect ratio, por lo que se establece
            canvas_height =  720;                   // un tamaÃ±o por defecto hasta poder calcular el tamaÃ±o final
            aspect_ratio_adjusted = false;
        }

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override
        {
            //aÃ±adido para el apect ratio
            state = LOADING;
            suspended = false;

            return true;
        }

        void suspend () override
        {
            suspended = true;
        }

        void resume () override
        {
            suspended = false;
        }

        void update (float ) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    };

}




#endif //REAL_NIGHTMARES_HELP_SCENE_HPP
