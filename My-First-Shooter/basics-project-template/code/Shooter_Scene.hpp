//
// Created by Esperanza on 19/01/2020.
//

#ifndef BASICS_PROJECT_TEMPLATE_SHOOTER_SCENE_HPP
#define BASICS_PROJECT_TEMPLATE_SHOOTER_SCENE_HPP

#endif //BASICS_PROJECT_TEMPLATE_SHOOTER_SCENE_HPP

#include <map> //para el mapa de texturas
#include <list> // para la lista de texturas
#include <memory> // para que guarde una memoria

#include <basics/Scene>//para poder crear una escena
#include <basics/Canvas>//para el uso de texturas
#include <basics/Texture_2D>
#include <basics/Id>
#include <basics/Timer>

namespace  shooter
{
    using basics::Canvas;
    using basics::Point2f;
    using basics::Size2f;
    using basics::Texture_2D;
    using basics::Graphics_Context;


class Shooter_Scene:public basics::Scene
    {

     typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
     typedef basics::Graphics_Context::Accessor Context;

        enum State
        {
            LOADING,
            READY,
            ERROR,
        };


        bool suspended;



    private:

        State state;

        unsigned  canvas_width;
        unsigned  canvas_height;







    public:

        Shooter_Scene();

        basics::Size2u get_view_size() override;

        bool initialize()override;

        void resume () override;

        void handle(basics::Event &event)override;

        void update(float time) override;

        void render(Graphics_Context::Accessor &context) override;







    };











}











