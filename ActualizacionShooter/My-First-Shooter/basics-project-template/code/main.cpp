﻿/*
 * MAIN
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <basics/Director>
#include <basics/enable>
#include <basics/Graphics_Resource_Cache>
#include <basics/Texture_2D>
#include <basics/Window>
#include <basics/opengles/Context>
#include "Intro_Scene.hpp"
#include <basics/opengles/Canvas_ES2>
#include <basics/opengles/OpenGL_ES2>


using namespace basics;
using namespace shooter;
using namespace std;


int main ()
{
    // Habilitamos un backend gráfico

    enable< basics::OpenGL_ES2 > ();

    // Cargamos la escena con el Director:

    director.run_scene (shared_ptr< Scene >(new Intro_Scene));

    return 0;
}
