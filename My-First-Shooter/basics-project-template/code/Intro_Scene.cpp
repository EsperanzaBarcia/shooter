//
// Created by Esperanza on 26/01/2020.
//

#include "Intro_Scene.hpp"
#include "Menu_Scene.hpp"
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace shooter
{

    // ---------------------------------------------------------------------------------------------

    bool Intro_Scene::initialize ()
    {
        if (state == UNINITIALIZED)
        {
            state = LOADING;
        }
        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Intro_Scene::update (float time)
    {
        if (!suspended) switch (state)
            {
                case LOADING:    update_loading    (); break;
                case WAITING:    update_waiting    (); break;
                default: break;
            }
    }

    // ---------------------------------------------------------------------------------------------

    void Intro_Scene::render (Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:

            if (canvas)
            {
                //se limpia el canvas
                canvas->clear ();

                //se pintan las texturas
                if (logo_texture)
                {

                    canvas->fill_rectangle
                            (
                                    { canvas_width * .5f, canvas_height * .5f },
                                    { logo_texture->get_width (), logo_texture->get_height () },
                                    logo_texture. get ()
                            );
                }

                //Se espera para pintar los siguientes sprites

                if(timer.get_elapsed_seconds() >1.f)
                {
                    if (hand1_texture)
                    {
                        canvas->fill_rectangle
                                (
                                        { canvas_width * .5f, canvas_height * .5f },
                                        { hand1_texture->get_width (), hand1_texture->get_height () },
                                        hand1_texture. get ()
                                );

                    }
                }
                if((timer.get_elapsed_seconds() >1.5f))
                {

                    if (hand2_texture)
                    {
                        canvas->fill_rectangle
                                (
                                        { canvas_width * .5f, canvas_height * .5f },
                                        { hand2_texture->get_width (), hand2_texture->get_height () },
                                        hand2_texture. get ()
                                );

                    }
                }

            }
        }
    }

    void Intro_Scene::update_loading ()
    {
        Graphics_Context::Accessor context = director.lock_graphics_context ();

        if (context)
        {
            // Se carga la textura del logo:

            logo_texture = Texture_2D::create (0, context, "vfx/logo.png");
            hand1_texture = Texture_2D::create (0, context, "vfx/hand1.png");
            hand2_texture = Texture_2D::create (0, context, "vfx/hand2.png");

            // Se comprueba si la textura se ha podido cargar correctamente:

            if (logo_texture)
            context->add (logo_texture);

            if (hand1_texture)
                context->add (hand1_texture);

            if(hand2_texture)
            {
                context->add (hand2_texture);

                //se pone el contador a 0
                timer.reset ();
                state   = WAITING;
            }

            else
                state   = ERROR;
        }
    }


    void Intro_Scene::update_waiting ()
    {
        // Se esperan unos segundos sin hacer nada:

        if (timer.get_elapsed_seconds () > 3.f)
        {
            timer.reset ();

            //y se carga la escena
            director.run_scene (shared_ptr< Scene >(new Menu_Scene));

        }
    }


}
