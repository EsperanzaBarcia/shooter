//
// Created by Esperanza on 18/01/2020.
//

#include "Menu_Scene.hpp"
#include "Game_Scene.hpp"
#include "Help_Scene.hpp"
#include <cstdlib>
#include <memory>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Director>

using namespace std;
using namespace basics;

namespace shooter
{
    Menu_Scene::Menu_Scene()
    {
        canvas_width= 1280;
        canvas_height= 720;
        state = LOADING;
        suspended =false;

    }

    bool Menu_Scene::initialize()
    {
        for(auto & option : options_array)
        {
            //las opciones no están pulsadas al empezar
            option.is_pressed =false;

        }


        return true;
    }


    void Menu_Scene::handle(basics::Event &event)
    {
        if(state == READY)
        {

            switch (event.id)
            {
                case ID(touch-started):

                case ID (touch-moved):
                {

                    //posición donde se ha tocado, tomando x e y, pasándolos a float
                    Point2f touch = {*event[ID(x)].as<var::Float>(), *event[ID(y)].as<var::Float>()};
                    int option_touched = point_at(touch);

                    //se activa solo la opción pulsada, las demás se desactivan
                    for(int i = 0; i < options_size;++i)
                    {
                        if(i == option_touched)
                        options_array[i].is_pressed = true;
                        else
                        options_array[i].is_pressed = false;

                    }
                    break;



                }
                case ID (touch-ended):
                {
                    //si se deja de tocar la pantalla también se sueltan las opciones
                    for( auto &option:options_array )
                        option.is_pressed = false;

                    //se analiza el último toque
                    Point2f touch = {*event[ID(x)].as<var::Float>(), *event[ID(y)].as<var::Float>()};

                    if(point_at(touch) == PLAY)
                    {
                        director.run_scene(shared_ptr< Scene >(new Game_Scene));
                    }

                    if(point_at(touch) == HELP)
                    {
                        director.run_scene(shared_ptr< Scene >(new Help_Scene));
                    }

                    break;

                }



            }


        }


    }


    void Menu_Scene::update(float time)
    {
        if(!suspended)
        {

            if(state == LOADING)
            {
                //se carga el contexto gráfico
                Graphics_Context::Accessor context = director.lock_graphics_context();

                //ajustamos el aspect ratio para que adapte la pantalla
                if(!aspect_ratio_adjusted)
                {
                    //al ser landscape width será mayor que height
                    float real_aspect_ratio = float( context->get_surface_width () ) / context->get_surface_height ();

                    canvas_width = unsigned( canvas_height * real_aspect_ratio );
                    aspect_ratio_adjusted = true;

                }
                if(context)
                {
                    //cuando haya contexto cargamos los atlas
                    MyTitleAtlas.reset(new Atlas("Atlas/Menu/Terror_atlas_titles.sprites",context));
                    MyAtlas.reset(new Atlas("Atlas/Menu/Terror_atlas.sprites",context));

                    //al estar cargados los atlas se cambia el estado
                    if(MyAtlas->good() &&  MyTitleAtlas->good())
                    {
                        state = READY;
                    }
                    else
                    {
                        state = ERROR;
                    }


                    if(state == READY)
                    {
                        //configuramos las opciones del menú
                        configure_menu();
                    }

                }

            }

        }

    }


    void Menu_Scene::render(basics::Graphics_Context::Accessor &context)
    {

        if(!suspended)
        {
            //si la aplicación no está suspendida llamamos al canvas
            Canvas * canvas = context->get_renderer<Canvas> (ID(Canvas));

            if(!canvas)
            {   //si el canvas no existe lo creamos
                canvas = Canvas::create((ID(Canvas)),context,{canvas_width,canvas_height});

            }

            if(canvas)
            {
                //si el canvas existe cargamos las opciones del atlas y el título
                canvas->clear();

                if(state == READY)
                {
                    for(auto &option: options_array)
                    {
                        canvas->set_transform
                        (
                           (
                                scale_then_translate_2d
                                (
                                   option.is_pressed ? 0.9f : 1.f,              // Escala de la opción
                                  { option.position[0], option.position[1] }      // Traslación

                                )

                            )

                        );

                        canvas->fill_rectangle({0.f,0.f},{option.slice->width,option.slice->height},option.slice,CENTER|TOP);

                    }

                    // Se restablece la transformación aplicada a las opciones para que no afecte a
                    // dibujos posteriores realizados con el mismo canvas:

                    canvas->set_transform (Transformation2f());


                    for(auto &slice: slices)
                    {
                        canvas->set_transform
                         (
                             (
                                 scale_then_translate_2d
                                  (
                                           1.f,              // Escala de la opción
                                          {slice.position[0], slice.position[1] }      // Traslación

                                  )

                             )

                         );

                        canvas->fill_rectangle({0.f,0.f},{slice.slice->width,slice.slice->height},slice.slice,CENTER|TOP);

                    }


                    // Se restablece la transformación aplicada a los títulos para que no afecte a
                    // dibujos posteriores realizados con el mismo canvas:

                    canvas->set_transform (Transformation2f());


                }



            }


        }





    }

    void Menu_Scene::configure_menu()
    {

        //tomamos los array y los asignamos a las opciones
        options_array[PLAY].slice = MyAtlas->get_slice(ID(play));
        options_array[HELP].slice = MyAtlas->get_slice(ID(help));

        slices[0].slice = MyTitleAtlas->get_slice(ID(real));
        slices[1].slice =  MyTitleAtlas->get_slice(ID(nightmares));


        //altura del menú
        float menu_height = 0;
        float menu_width= options_array[0].slice->width;

        //se suman los tamaños para calcular la altura del menú
        for(auto & option: options_array)
        {
            menu_height += option.slice->height;
        }


        //posición de la opción superior
        float option_top = canvas_height/2.f + menu_height/2.f ;


        //posición del menú en cuando al width para ponerlo a la derecha
        for(int i = 0 ; i < options_size; ++i)
        {
            options_array[i].position = Point2f{canvas_width - menu_width,option_top};

            option_top -= options_array[i].slice->height;

        }

        //posición de cada elemento del título
        for(int i = 0 ; i < title_slices; ++i)
        {
            slices[i].position = Point2f{canvas_width/3 ,canvas_height/2 + 100 - i*70 };
        }


        initialize(); //se restablecen las opciones

    }


    int Menu_Scene::point_at(basics::Point2f &point)
    {
        for(int i = 0; i < options_size;++i)
        {
            const Option & option = options_array[i];
            //si el point se encuentra dentro de alguna de las opciones devuelve su índice
            if( point[0] > option.position[0] - option.slice->width  &&
                point[0] < option.position[0] + option.slice->width  &&
                point[1] > option.position[1] - option.slice->height &&
                point[1] < option.position[1] + option.slice->height )
            {
                return i;
            }
        }

        return -1;


    }








}
