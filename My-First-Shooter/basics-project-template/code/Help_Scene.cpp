//
// Created by Esperanza on 26/01/2020.
//

#include "Menu_Scene.hpp"
#include "Help_Scene.hpp"
#include <basics/Canvas>
#include <basics/Director>
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace basics;
using namespace std;

namespace shooter
{

    void Help_Scene::update (float time)
    {

        // Mientras se estÃ¡ cargando la escena, cuando se consigue acceso al contexto grÃ¡fico por
        // primera vez, es cuando se puede calcular el aspect ratio:
        if(state == LOADING)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if(context)
            {
                enemy1_texture = Texture_2D::create (0, context, "Characters/demonio.png");
                enemy2_texture = Texture_2D::create (0, context, "Characters/Hola.png");

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (enemy1_texture)
                    context->add (enemy1_texture);

                if (enemy2_texture)
                    context->add (enemy2_texture);



                if(!aspect_ratio_adjusted)
                {

                    // En este ejemplo la ventana estÃ¡ bloqueada para permanecer horizontal.
                    // Por tanto, lo normal es que el ancho sea mayor que el alto. El alto de la resoluciÃ³n
                    // virtual se va a dejar fijo en 720 unidades (tal como se estableciÃ³ en el constructor)
                    // y se va a escalar el ancho de la resoluciÃ³n virtual para que el aspect ratio virtual
                    // coincida con el real de la ventana:
                    float real_aspect_ratio = float( context->get_surface_width () ) / context->get_surface_height ();

                    canvas_width = unsigned( canvas_height * real_aspect_ratio );
                    aspect_ratio_adjusted = true;

                }




                else if(!font)
                {
                    font.reset (new Raster_Font("Fonts/impact.fnt", context));


                }
                else{
                    state = READY;
                }
            }



        }




    }

    void Help_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();

                if(state == READY)
                {

                    //se pintan las texturas
                    if (enemy1_texture)
                    {

                        canvas->fill_rectangle
                                (
                                        { 100.f, 100.f },
                                        {enemy1_texture->get_width (), enemy1_texture->get_height () },
                                        enemy1_texture. get ()
                                );
                    }
                    if (enemy2_texture)
                    {

                        canvas->fill_rectangle
                                (
                                        { 400.f,100.f},
                                        {enemy2_texture->get_width (), enemy2_texture->get_height () },
                                        enemy2_texture. get ()
                                );
                    }



                    //TEXTOS

                    if (font)
                    {

                        // Se dibujan textos con diferentes puntos de anclaje a partir de una cadena simple:

                        Text_Layout description_text(*font, L"It is halloween night");
                        Text_Layout description2_text(*font, L"Shoot the enemies to defend your home from the monsters");
                        Text_Layout description3_text(*font, L"Enemies examples:");


                        canvas->draw_text ({     0.f , canvas_height -margin*2}, description_text,    LEFT);
                        canvas->draw_text ({     0.f , canvas_height   - margin *4    }, description2_text, LEFT);
                        canvas->draw_text ({     0.f , canvas_height   - margin *6    }, description3_text, LEFT);


                    }









                }

            }
        }
    }

}
