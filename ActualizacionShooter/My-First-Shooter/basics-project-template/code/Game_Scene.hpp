//
// Created by Esperanza on 25/01/2020.
//

#ifndef REAL_NIGHTMARES_GAME_SCENE_HPP
#define REAL_NIGHTMARES_GAME_SCENE_HPP

#include <map> //para el mapa de texturas
#include <list> // para la lista de texturas
#include <memory> // para que guarde una memoria

#include <basics/Scene>//para poder crear una escena
#include <basics/Canvas>//para el uso de texturas
#include <basics/Texture_2D>
#include <basics/Id>
#include <basics/Timer>
#include "Sprite.hpp"
#include "Game_Scene.hpp"

namespace shooter {

    using basics::Id;
    using basics::Timer;
    using basics::Canvas;
    using basics::Texture_2D;


    class Game_Scene:public basics::Scene {

        typedef std::shared_ptr<Sprite> Sprite_Handle;
        typedef std::list<Sprite_Handle> Sprite_List;
        typedef std::shared_ptr<Texture_2D> Texture_Handle;
        typedef std::map<Id, Texture_Handle> Texture_Map;
        typedef basics::Graphics_Context::Accessor Context;

        ///Estado de la escena
        enum State {
            LOADING,
            RUNNING,
            ERROR,
        };


        enum Gameplay_State {

            PLAYING,
            PAUSED,
            FINISHED
        };


        bool suspended;

        Vector2f player_speed = {0.f,0.f};

        unsigned i = 0;



    private:

        ///Array de struct con la información de las texturas (Id y ruta) que hay que cargar.
        static struct Texture_Data {
            Id id;
            const char *path;
        }textures_data[];

        ///Número de items que hay en el array textures_data.
        static unsigned textures_count;


    private:

        ///estado de la escena
        State state;

        Gameplay_State game_state;
        unsigned canvas_width;
        unsigned canvas_height;


        Texture_Map texturemap;                            /// Mapa  en el que se guardan shared_ptr a las texturas cargadas.
        Sprite_List sprites_array;                             /// Lista en la que se guardan shared_ptr a los sprites creados.

        Sprite * player_sprite;                          ///< Puntero al sprite de la lista de sprites que representa

        Sprite * pause_sprite;                          ///< Puntero al sprite de la lista de sprites que representa
        Sprite * pause_panel;                          ///< Puntero al sprite de la lista de sprites que representa




         ///<Struct de enemigos ya que se van a tratar independientemente
        struct Enemy
        {
            Sprite * enemy_sprite;
            float enemy_speed = 0.f;

        };

        static const unsigned enemies_size = 3;
        Enemy enemies[enemies_size];


        Timer timer;                               ///< Cronómetro usado para medir intervalos de tiempo


        ///<Struct de disparos ya que se van a tratar independientemente
        struct Shot
        {
            Sprite * sprite;
            Vector2f shot_target = {0.f,0.f};//empeiza en 0
            float shot_speed = 500.f;
            bool followingtarget;

        };

        static const unsigned shot_size = 3;
        Shot shots[shot_size];




        //CONFIGURACIÓN DEL ASPECT RATIO
        bool     aspect_ratio_adjusted;             /// false hasta que se ajuste el aspect ratio de la resolución
                                                     ///virtual para que concincida con el de la real


    public:

        Game_Scene();

        basics::Size2u get_view_size() override
        {
            return {canvas_width,canvas_height};
        }

        bool initialize() override;

        void suspend() override;

        void resume() override;

        void handle(basics::Event &event) override;

        void update(float time) override;

        void render(Context &context) override;


    public:


        void load_textures ();

        void configure_sprites ();

        void set_initial_positions ();


        void run(float time);


        void render_loading(Canvas & canvas);

        void render_playfield(Canvas &canvas);

        void render_pause(Canvas & canvas);


        //mías
        void place_enemies(Sprite *sprite);

        void move_enemies();

        bool check_shot_collisions(Sprite *sprite);

        void check_enemies_collisions(Sprite *sprite);





    };


}







#endif
