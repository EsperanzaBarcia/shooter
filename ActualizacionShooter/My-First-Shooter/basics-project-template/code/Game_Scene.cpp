//
// Created by Esperanza on 25/01/2020.
//

#include "Game_Scene.hpp"
#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Transformation>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace shooter
{
    /// ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    /// carga está la primera para poder dibujarla cuanto antes:

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
            {
                    { ID(loading),       "Characters/Fan350.png"   },
                    { ID(player),        "Peques/player.png"   },
                    { ID(shot),          "Peques/Rock.png"   },
                    { ID(enemy),         "Peques/demonio.png"   },
                    { ID(pause),         "test.png"   },

            };

    // Pâra determinar el número de items en el array textures_data, se divide el tamaño en bytes
    // del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);



    Game_Scene::Game_Scene()
    {
        canvas_width= 1280;
        canvas_height= 720;


    }

    bool Game_Scene::initialize()
    {
        state = LOADING;
        game_state = PLAYING;
        suspended = true;
        i = 0;

        return true;
    }



    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
    }



    void Game_Scene::handle (Event & event)
    {
        if (state == RUNNING)               // Se descartan los eventos cuando la escena está LOADING
        {

          switch (event.id)
                {
                    case ID(touch-started):// El usuario toca la pantalla
                    case ID(touch-moved):
                    {

                        //actualiza el target solo si el disparo está en la posición inicial

                        Vector2f touch = {*event[ID(x)].as<var::Float>(),
                                          *event[ID(y)].as<var::Float>()};

                        if(pause_sprite->contains(touch) && game_state!= PAUSED)
                        {
                            game_state = PAUSED;
                        }
                        else{
                            game_state = PLAYING;
                        }

                        if(i < shot_size)
                        {
                            //si el disparo no ha sido disparado aún, por tanto está en su posición inicial
                            if(shots[i].sprite->get_position() == player_sprite->get_position())
                            {
                                shots[i].shot_target = touch;
                                shots[i].followingtarget = true;
                            }

                        }

                        break;
                    }

                    case ID(touch-ended):
                    {
                       shots[i].followingtarget =false;
                       if(i >= shot_size)
                           i=0;
                       else
                        i++;

                        break;
                    }// El usuario deja de tocar la pantalla

                }
        }
    }


    void Game_Scene::update (float time)
    {
        if (!suspended) switch (state)
            {
                case LOADING: load_textures  ();     break;
                case RUNNING: run(time); break;
                case ERROR:   break;
            }
    }

    void Game_Scene::render(Context &context)
    {

        if(!suspended)
        {
            Canvas * canvas =  context->get_renderer< Canvas > (ID(canvas));


            if(!canvas)
            {
              canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});

            }


            if(canvas)
            {
                canvas->clear();

               if(state == LOADING)
               {
                   render_loading(*canvas);
               }
               if(state == RUNNING)
               {
                   if(game_state ==PAUSED)
                       render_pause(*canvas);

                   if(game_state == PLAYING)
                   render_playfield(*canvas);

                   if(game_state == FINISHED)
                       director.run_scene(shared_ptr< Scene >(new Game_Scene));



               }



            }




        }


    }

    void Game_Scene::load_textures()
    {

        if (texturemap.size () < textures_count)          // Si quedan texturas por cargar
        {
            // Las texturas se cargan y se suben al contexto gráfico, por lo que es necesario disponer
            // de uno:

            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {

                // Se carga la siguiente textura (textures.size() indica cuántas llevamos cargadas):

                Texture_Data   & texture_data = textures_data[texturemap.size ()];
                Texture_Handle & texture      = texturemap[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (texture) context->add (texture); else state = ERROR;

                // Cuando se han terminado de cargar todas las texturas se pueden crear los sprites que
                // las usarán e iniciar el juego:
            }
        }
        else
        if (timer.get_elapsed_seconds () > 1.5f)
        {
            configure_sprites ();
            set_initial_positions();

            state = RUNNING;
        }


    }


    void Game_Scene::configure_sprites()
    {
        //congifurar sprites del fondo


        // Se crean y configuran los sprites

        Sprite_Handle    player_handle(new Sprite( texturemap[ID(player)].get () ));

        Sprite_Handle    enemy_handle(new Sprite( texturemap[ID(enemy)].get () ));
        Sprite_Handle    enemy_handle2(new Sprite( texturemap[ID(enemy)].get () ));
        Sprite_Handle    enemy_handle3(new Sprite( texturemap[ID(enemy)].get () ));

        Sprite_Handle    shot_handle(new Sprite( texturemap[ID(shot)].get () ));
        Sprite_Handle    shot_handle2(new Sprite( texturemap[ID(shot)].get () ));
        Sprite_Handle    shot_handle3(new Sprite( texturemap[ID(shot)].get () ));

        Sprite_Handle    pause_handle(new Sprite( texturemap[ID(pause)].get () ));




        sprites_array.push_back (player_handle);

        sprites_array.push_back (enemy_handle);
        sprites_array.push_back (enemy_handle2);
        sprites_array.push_back (enemy_handle3);

        sprites_array.push_back (shot_handle);
        sprites_array.push_back (shot_handle2);
        sprites_array.push_back (shot_handle3);

        sprites_array.push_back (pause_handle);



        // Se guardan punteros a los sprites que se van a usar frecuentemente:

        player_sprite    =        player_handle.get ();

        pause_sprite    =        pause_handle.get ();

        enemies[0].enemy_sprite  =   enemy_handle.get();
        enemies[1].enemy_sprite  =   enemy_handle2.get();
        enemies[2].enemy_sprite  =   enemy_handle3.get();

        shots[0].sprite    =         shot_handle.get();
        shots[1].sprite    =         shot_handle2.get();
        shots[2].sprite    =         shot_handle3.get();

    }

    void Game_Scene::set_initial_positions()
    {
        pause_sprite->set_position({canvas_width-50.f,canvas_height-50.f });


        //jugador
        player_sprite->set_position({canvas_width/2,canvas_height/2});

        for(int i = 0 ; i < shot_size;++i)
        {
            shots[i].sprite->set_position(player_sprite->get_position());
            shots[i].sprite->hide();
        }


        //enemigos
        for(int i = 0 ; i < enemies_size;++i)
        {
            place_enemies(enemies[i].enemy_sprite);
            enemies[i].enemy_speed =100.f;
        }
        //comienza a mover a los enemigos
        move_enemies();


        //el juego comienza

        game_state = PLAYING;

    }


    void Game_Scene::run(float time)
    {
        Vector2f player_position = player_sprite->get_position();



        for(int i = 0 ; i< shot_size;++i)
        {
            if(!check_shot_collisions(shots[i].sprite))
            {
                if(shots[i].followingtarget)
                {
                    shots[i].sprite->show();
                    Vector2f shot_position = shots[i].sprite->get_position();
                    Vector2f direction = shots[i].shot_target - shot_position;
                    shots[i].sprite->set_speed(direction.normalized() * shots[i].shot_speed);

                }
            }
        }

///
        move_enemies();
        ///


        check_enemies_collisions(player_sprite);




        //Llamamos a los update de todos los sprites
        for(int i=0; i < enemies_size;++i)
        {
            enemies[i].enemy_sprite->update(time);

        };

        //Llamamos al update de todos los enemigos
        for(int i=0; i < enemies_size;++i)
        {

            shots[i].sprite->update(time);

        };

        //llamamos al update del player
         player_sprite->update(time);


    }



    void Game_Scene::render_loading(basics::Canvas &canvas)
    {
        Texture_2D * loading_texture = texturemap[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
                    (
                            { canvas_width * .5f, canvas_height * .5f },
                            { loading_texture->get_width (), loading_texture->get_height () },
                            loading_texture
                    );


        }
    }


    void Game_Scene::render_playfield(basics::Canvas &canvas)
    {


        if(game_state != FINISHED)
        {

            for (auto & sprite : sprites_array)
            {
                sprite->render (canvas);

            }

        }

    }


    void Game_Scene::render_pause(basics::Canvas &canvas)
    {

        pause_sprite->show();
        canvas.fill_rectangle({0.f,0.f},{canvas_width-10.f,canvas_height-10.f});




    }




    void Game_Scene::place_enemies(Sprite *sprite)
    {
            //asignar posicion
            float r = rand() % 100;

            //si es 0 que venga desde posicion aleatoria desde arriba
            if(r < 25)
            {
                sprite->set_position({r,canvas_height+50.f});
            }
                //si es 1 que venga desde la derecha aleatoria
            else if(r >= 25 && r < 50)
            {
                sprite->set_position({canvas_width+50.f,r});
            }
                //si es 2 que venga desde la izquierda aleatoria
            else if(r >=50 && r < 75)
            {
                sprite->set_position({0.f-50.f,r});
            }
                //si es 3 que venga desde abajo aleatorio
            else if(r >= 75 && r < 100)
            {
                sprite->set_position({r,0.f-50.f});
            }

            sprite->show();

    }


    void Game_Scene::move_enemies()
    {
        Vector2f player_position = player_sprite->get_position();

        for(int i=0; i < enemies_size;++i)
        {

            //asignar target
            Vector2f enemy_position = enemies[i].enemy_sprite->get_position();
            Vector2f direction = player_position - enemy_position;
            enemies[i].enemy_sprite->set_speed(direction.normalized() * enemies[i].enemy_speed);


        };



    }


    bool Game_Scene::check_shot_collisions(Sprite * sprite)
    {
        if(sprite->get_position_y() >= canvas_height ||sprite->get_position_x() >= canvas_width
           ||sprite->get_position_y() <= 0 ||sprite->get_position_x() <= 0)
        {
            sprite->hide();
            sprite->set_position(player_sprite->get_position());
            return true;
        }
        else
           for(int i = 0; i < enemies_size;++i)
           {
               if(sprite->intersects(*enemies[i].enemy_sprite))
               {
                   //recolocamos el disparo
                   sprite->hide();
                   sprite->set_position(player_sprite->get_position());

                   //recolocamos aleatoriamente el enemigo
                   enemies[i].enemy_sprite->hide();
                   place_enemies(enemies[i].enemy_sprite);

               }
           }


        return false;

    }

    void Game_Scene::check_enemies_collisions(Sprite * sprite)
    {
            for(int i = 0; i < enemies_size;++i)
            {
                if(enemies[i].enemy_sprite->intersects(*player_sprite))
                {
                    sprite->hide();
                    enemies[i].enemy_sprite->hide();
                    player_sprite->hide();
                    game_state = FINISHED;

                }
            }




    }






}
